const {v4} = require("uuid");
const {getCertificateInfo} = require('openssl-cert-tools');
const {readFile, unlinkSync} = require('fs');
const utf8 = require('utf8');


const spawn = require('child_process').spawn;

class SslInfo {
  async convertation(filePath) {
    return new Promise((resolve, reject) => {
      const newFileName = `${v4()}.pem`;

      const openssl = spawn('openssl', ['pkcs7', '-print_certs', '-in', filePath, '-out', newFileName]);

      openssl.on('close', function (code) {
        return resolve(newFileName)
      });
    })
  }

  async fileRead(filePath) {
    return new Promise((resolve, reject) => {
      readFile(filePath, "utf8", function (error, data) {
        if (error) {
          return reject(error);
        }
        return resolve(data);
      });
    });
  }

  async readSslInfo(certificate) {
    return new Promise((resolve, reject) => {
      getCertificateInfo(certificate, function (error, data) {
        if (error) {
          return reject(error);
        }
        return resolve(data);
      });
    });
  }

  async getSslInfo(filePath) {
    const convertName = await this.convertation(filePath);

    try {
      const certificate = await this.fileRead(convertName);

      if (!certificate) {
        throw new Error('File read error');
      }

      let certs = certificate.split('-----END CERTIFICATE-----\n\n');

      if(certs.length > 1) {
        for(let i = 0; i < certs.length - 1; i++) {
          certs[i] += '-----END CERTIFICATE-----\n\n';
        }
      }

      const results = await Promise.all(certs.filter(Boolean).map((cert) => this.readSslInfo(cert)));
      results.forEach(result => {
        Object.keys(result.subject).forEach(key => {
          const utf8s = result.subject[key].replace(/\\([0-9A-F]{2})/g, "\\x$1");
          result.subject[key] = utf8.decode(eval("'" + utf8s + "'"))
        });
        return result;
      });
      console.log(results)
      return JSON.stringify(results);
    } catch (e) {
      throw e;
    } finally {
      unlinkSync(convertName);
    }
  }
}

const sslInfo = new SslInfo();

const [_, __, sslFilePath] = process.argv;

if (!sslFilePath) {
  throw new Error(`'node index.js FILE_PATH' - argument 'FILE_PATH' is required!`);
}

sslInfo.getSslInfo(sslFilePath);//.then(console.log).catch(console.error);